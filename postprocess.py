import comet_ml
import pandas as pd
import matplotlib.pyplot as plt

METRICS = ["accuracy", "batch_loss"]


def summary_metrics_markdown(
    workpspace, project_name, experiment, project_df, candidate_df
):
    md = (
        f"### Project: {project_name} Summary Metrics\n"
        f"{project_df.T.to_markdown()}\n"
        f"\n"
        f"Candidate: [{experiment.id[:8]}]({experiment.url})\n"
        f"{candidate_df.to_markdown(index=False)}"
    )
    return md


def extract_table_row(metrics):
    table_row = {}
    for metric in metrics:
        column = metric["name"]
        if column not in METRICS:
            continue
        value = float(metric["valueCurrent"])

        table_row[column] = value

    return table_row


def create_summary_metrics_md(workspace, project_name, experiment_id):
    api = comet_ml.API()
    experiments = api.get(f"{workspace}/{project_name}")

    project_metric_summaries = list(
        map(lambda x: extract_table_row(x.get_metrics_summary()), experiments)
    )
    project_df = pd.DataFrame(project_metric_summaries)
    project_df_summary = project_df.describe(percentiles=[0.05, 0.25, 0.5, 0.75, 0.95])

    candidate = api.get(f"{workspace}/{project_name}/{experiment_id}")
    candidate_metrics = extract_table_row(candidate.get_metrics_summary())
    candidate_df = pd.DataFrame(candidate_metrics, index=[0])

    metrics_md = summary_metrics_markdown(
        workspace, project_name, candidate, project_df_summary, candidate_df
    )

    return metrics_md


def create_accuracy_plot(workspace, project_name, experiment_id):
    api = comet_ml.API()
    experiment = api.get(f"{workspace}/{project_name}/{experiment_id}")

    accuracy = experiment.get_metrics("accuracy")
    steps_accuracy = [
        (model["step"], float(model["metricValue"])) for model in accuracy
    ]
    steps, acc = zip(*steps_accuracy[:100])

    fig, ax = plt.subplots()
    ax.plot(steps, acc, "-b", label="Accuracy")
    ax.legend()

    return fig


def create_mr_artifacts(path, workspace, project_name, experiment_id):
    summary_metrics_md = create_summary_metrics_md(
        workspace, project_name, experiment_id
    )
    with open(f"{path}/summary.md", "w") as f:
        f.write(summary_metrics_md)

    fig = create_accuracy_plot(workspace, project_name, experiment_id)
    fig.savefig(f"{path}/model_accuracy.png")


def get_experiment_id(workspace, project_name, model_name):
    """Fetch the id of the latest experiment that logged a model"""

    try:
        api = comet_ml.API(api_key=os.environ["COMET_API_KEY"])
    except:
        print("Using API Key local")
        api = comet_ml.API()
    experiments = api.get(str(workspace), str(project_name))

    # Filter only the experiments that logged a model
    model_experiments = [
        experiment
        for experiment in experiments
        if experiment.get_model_asset_list(model_name)
    ]
    # Sort Experiments based on Timestamp
    model_experiments = sorted(
        model_experiments, key=lambda x: x.end_server_timestamp, reverse=True
    )

    latest_model_experiment = model_experiments[0]
    experiment_id = latest_model_experiment.id

    return experiment_id
